﻿using UnityEngine;
using UnityEditor;

public sealed class StalkGridView : ScriptableWizard
{
    [MenuItem("Helpers/View Stalk Grid")]
    static private void Run()
    {
        ScriptableWizard.DisplayWizard<StalkGridView>("View Stalk Grid");
    }

    [SerializeField]
    private Stalk[] stalks = null;

    private void OnWizardCreate()
    {
        foreach (Stalk stalk in this.stalks)
        {
            stalk.transform.localScale = Vector3.one;
            stalk.Body.localScale = new Vector3(0.8f, stalk.Height, 0.8f);

            foreach (Transform child in stalk.Body)
            {
                child.localScale = Vector3.one;
            }
        }
    }
}
