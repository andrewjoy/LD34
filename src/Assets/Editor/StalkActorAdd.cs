﻿using UnityEngine;
using UnityEditor;

public sealed class StalkActorAdd : ScriptableWizard
{
    [MenuItem("Helpers/Add Stalk Actor")]
    static private void Run()
    {
        ScriptableWizard.DisplayWizard<StalkActorAdd>("Add Stalk Actor", "Add!");
    }

    [SerializeField]
    private Stalk stalk = null;

    [SerializeField]
    private Actor actorPrefab = null;

    private void OnWizardCreate()
    {
        Actor newActor = (Actor)PrefabUtility.InstantiatePrefab(this.actorPrefab);

        newActor.AttachedStalk = this.stalk;

        newActor.transform.position = this.stalk.AttachPoint.position;
        newActor.transform.rotation = this.stalk.AttachPoint.rotation;
    }
}
