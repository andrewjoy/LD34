﻿using UnityEngine;
using UnityEditor;

public sealed class StalkGridBuilder : ScriptableWizard
{
    [MenuItem("Helpers/Create Stalk Grid")]
    static private void Run()
    {
        ScriptableWizard.DisplayWizard<StalkGridBuilder>("Create Stalk Grid");
    }

    [SerializeField]
    private Transform root = null;

    [SerializeField]
    private Stalk prefab = null;

    [SerializeField]
    private int size = 15;

    private void OnWizardCreate()
    {
        Stalk[,] grid = new Stalk[this.size, this.size];

        for (int x = 0; x < this.size; ++x)
        {
            for (int y = 0; y < this.size; ++y)
            {
                grid[x, y] = (Stalk)PrefabUtility.InstantiatePrefab(this.prefab);
                grid[x, y].transform.position = new Vector3(x, 0.0f, y);
                grid[x, y].transform.rotation = Quaternion.identity;

                grid[x, y].transform.parent = this.root;

                grid[x, y].gameObject.name = string.Concat(this.prefab.name, " x:", x, " y:", y);
            }
        }

        for (int x = 0; x < this.size; ++x)
        {
            for (int y = 0; y < this.size; ++y)
            {
                if (x > 0)
                {
                    grid[x, y].SetNeighbourMutual(Stalk.NeighbourDirection.West, grid[x - 1, y]);
                }

                if (x < this.size - 1)
                {
                    grid[x, y].SetNeighbourMutual(Stalk.NeighbourDirection.East, grid[x + 1, y]);
                }

                if (y > 0)
                {
                    grid[x, y].SetNeighbourMutual(Stalk.NeighbourDirection.South, grid[x, y - 1]);
                }

                if (y < this.size - 1)
                {
                    grid[x, y].SetNeighbourMutual(Stalk.NeighbourDirection.North, grid[x, y + 1]);
                }
            }
        }
    }
}
