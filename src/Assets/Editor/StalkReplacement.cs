﻿using UnityEngine;
using UnityEditor;

public sealed class StalkReplacement : ScriptableWizard
{
    [MenuItem("Helpers/Replace Stalk")]
    static private void Run()
    {
        ScriptableWizard.DisplayWizard<StalkReplacement>("Replace Stalk", "Replace!");
    }

    [SerializeField]
    private Stalk stalkToBeReplaced = null;

    [SerializeField]
    private Stalk stalkPrefab = null;

    private void OnWizardCreate()
    {
        Stalk newStalk = (Stalk)PrefabUtility.InstantiatePrefab(this.stalkPrefab);

        newStalk.Water = this.stalkToBeReplaced.Water;

        newStalk.transform.parent = this.stalkToBeReplaced.transform.parent;

        newStalk.transform.localPosition = this.stalkToBeReplaced.transform.localPosition;
        newStalk.transform.localRotation = this.stalkToBeReplaced.transform.localRotation;
        newStalk.transform.localScale = this.stalkToBeReplaced.transform.localScale;
        
        // Unity is a terrible fucking engine
        SerializedObject serializedObject = null;

        serializedObject = new SerializedObject(this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.North));
        serializedObject.FindProperty("southNeighbour").objectReferenceValue = newStalk;

        newStalk.SetNeighbourMutual(Stalk.NeighbourDirection.North, this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.North));

        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();

        serializedObject = new SerializedObject(this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.South));
        serializedObject.FindProperty("northNeighbour").objectReferenceValue = newStalk;

        newStalk.SetNeighbourMutual(Stalk.NeighbourDirection.South, this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.South));

        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();

        serializedObject = new SerializedObject(this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.East));
        serializedObject.FindProperty("westNeighbour").objectReferenceValue = newStalk;

        newStalk.SetNeighbourMutual(Stalk.NeighbourDirection.East, this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.East));

        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();

        serializedObject = new SerializedObject(this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.West));
        serializedObject.FindProperty("eastNeighbour").objectReferenceValue = newStalk;

        newStalk.SetNeighbourMutual(Stalk.NeighbourDirection.West, this.stalkToBeReplaced.GetNeighbour(Stalk.NeighbourDirection.West));

        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();

        GameObject.DestroyImmediate(this.stalkToBeReplaced.gameObject);
    }
}
