﻿using System.Collections.Generic;

using UnityEngine;

using Slerpy;

public sealed class Actor : MonoBehaviour
{
    [SerializeField]
    private float movementTime = 0.25f;

    [SerializeField]
    private Stalk attachedStalk = null;

    private readonly Queue<Stalk> movementTargets = new Queue<Stalk>();
    private float timeMoving = 0.0f;

    public Stalk AttachedStalk
    {
        get
        {
            return this.attachedStalk;
        }

        set
        {
            if (this.attachedStalk != value)
            {
                this.attachedStalk = value;

                this.movementTargets.Enqueue(this.attachedStalk);
            }
        }
    }

    public bool Move(Stalk.NeighbourDirection direction)
    {
        Stalk newAttachedStalk = this.CalculateMovementTarget(direction);
        if (newAttachedStalk != this.AttachedStalk)
        {
            this.AttachedStalk = newAttachedStalk;

            return true;
        }

        return false;
    }

    public void MoveTo(Stalk newAttachedStalk)
    {
        this.AttachedStalk = newAttachedStalk;
    }

    public Stalk CalculateMovementTarget(Stalk.NeighbourDirection direction)
    {
        if (this.attachedStalk != null)
        {
            Stalk newAttachedStalk = this.attachedStalk;
            Stalk potentialAttachedStalk = newAttachedStalk;

            int heightRemaining = this.attachedStalk.Height;
            while ((potentialAttachedStalk = potentialAttachedStalk.GetNeighbour(direction)) != null 
                && potentialAttachedStalk.Height <= heightRemaining + 1
                && heightRemaining-- >= 0)
            {
                if (potentialAttachedStalk.Height > 0)
                {
                    newAttachedStalk = potentialAttachedStalk;

                    if (newAttachedStalk.Height > heightRemaining)
                    {
                        break;
                    }
                }
            }

            return newAttachedStalk;
        }

        return null;
    }

    private void Start()
    {
        if (this.attachedStalk != null)
        {
            this.movementTargets.Enqueue(this.attachedStalk);
        }
    }

    private void LateUpdate()
    {
        if (this.movementTargets.Count > 1)
        {
            IEnumerator<Stalk> movementTargetsEnumerator = this.movementTargets.GetEnumerator();

            movementTargetsEnumerator.MoveNext();
            Stalk from = movementTargetsEnumerator.Current;

            movementTargetsEnumerator.MoveNext();
            Stalk to = movementTargetsEnumerator.Current;

            float weight = Weight.FromTime(WrapType.Clamp, this.timeMoving, this.movementTime);
            weight = Weight.WithType(WeightType.Eager, weight);

            Vector3 centerPoint = (from.AttachPoint.position + to.AttachPoint.position) * 0.5f;

            centerPoint -= new Vector3(0.0f, 0.1f, 0.0f);

            this.transform.position = Vector3.Slerp(from.AttachPoint.position - centerPoint, to.AttachPoint.position - centerPoint, weight) + centerPoint;

            this.timeMoving += Time.deltaTime;

            if (this.timeMoving >= this.movementTime)
            {
                this.timeMoving -= this.movementTime;

                this.movementTargets.Dequeue();
            }
        }
        else
        {
            this.timeMoving = 0.0f;

            this.transform.position = this.movementTargets.Peek().AttachPoint.position;
        }
    }
}
