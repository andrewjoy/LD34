﻿using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

public sealed class HungryStalk : Stalk
{
    [SerializeField]
    private AudioSource killSound = null;

    [SerializeField]
    private Water waterPrefab = null;

    private void OnTriggerEnter(Collider otherCollider)
    {
        Bug otherBug = (otherCollider.attachedRigidbody == null ? otherCollider.gameObject : otherCollider.attachedRigidbody.gameObject).GetComponent<Bug>();
        if (otherBug != null)
        {
            TransformEffect killEffect = otherBug.gameObject.AddComponent<TransformEffect>();

            killEffect.SetWeights(WeightType.Heavy);
            killEffect.Duration = 0.2f;
            killEffect.TimeWrap = WrapType.Clamp;
            killEffect.ScaleExtent = -new Vector3(1.0f, 1.0f, 1.0f);

            Component.Destroy(otherBug);
            GameObject.Destroy(otherBug.gameObject, killEffect.Duration);

            Water water = (Water)Component.Instantiate(this.waterPrefab, this.transform.position, this.transform.rotation);
            water.Actor.AttachedStalk = this;

            water = (Water)Component.Instantiate(this.waterPrefab, this.AttachPoint.position, this.transform.rotation);
            water.Actor.AttachedStalk = this;

            this.killSound.PlayOneShot(this.killSound.clip);

            int randomOffset = Random.Range(0, 4);
            for (int i = 0; i < 4; ++i)
            {
                if (water.Actor.Move((Stalk.NeighbourDirection)((i + randomOffset) % 4)))
                {
                    return;
                }
            }
        }
    }
}
