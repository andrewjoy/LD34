﻿using UnityEngine;

[RequireComponent(typeof(Actor))]
public sealed class Bug : MonoBehaviour
{
    [SerializeField]
    private AudioSource killSound = null;

    [SerializeField]
    private float moveInterval = 0.5f;

    [SerializeField]
    private Stalk.NeighbourDirection[] path = null;

    private int currentPathIndex = 0;

    public Actor Actor { get; private set; }

    private void Awake()
    {
        this.Actor = this.gameObject.GetComponent<Actor>();
    }

    private void Update()
    {
        if (Time.time % this.moveInterval < Time.deltaTime)
        {
            int wrappedPathIndex = this.currentPathIndex % (this.path.Length * 2);

            ++this.currentPathIndex;

            if (wrappedPathIndex >= this.path.Length)
            {
                wrappedPathIndex = this.path.Length - (wrappedPathIndex - this.path.Length) - 1;

                if (this.Actor.Move(this.path[wrappedPathIndex].Flip()))
                {
                    return;
                }
            }
            else
            {
                if (this.Actor.Move(this.path[wrappedPathIndex]))
                {
                    return;
                }
            }
            
            // movement failed
            this.currentPathIndex = this.path.Length - (wrappedPathIndex - this.path.Length);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Player otherPlayer = (collision.rigidbody == null ? collision.gameObject : collision.rigidbody.gameObject).GetComponent<Player>();
        if (otherPlayer != null)
        {
            otherPlayer.LoseLevel();

            this.killSound.PlayOneShot(this.killSound.clip);
        }
    }
}
