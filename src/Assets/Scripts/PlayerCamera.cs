﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public sealed class PlayerCamera : MonoBehaviour
{
    [SerializeField]
    private Player player = null;

    [SerializeField]
    private float heightOffset = 25.0f;

    public Player Player
    {
        get
        {
            return this.player;
        }
    }

    public float HeightOffset
    {
        get
        {
            return this.heightOffset;
        }

        set
        {
            this.heightOffset = value;
        }
    }

    private void Update()
    {
        this.transform.position = new Vector3(
            this.transform.position.x, 
            Mathf.Lerp(this.transform.position.y, this.player.transform.position.y + this.heightOffset, Time.deltaTime), 
            this.transform.position.z);
    }
}
