﻿using UnityEngine;

public interface IWaterable
{
    int Water { get; set; }
}

[RequireComponent(typeof(Actor))]
public sealed class Water : MonoBehaviour
{
    [SerializeField]
    private bool affectPlayerOnly = false;

    public Actor Actor { get; private set; }

    private void Awake()
    {
        this.Actor = this.gameObject.GetComponent<Actor>();
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        GameObject otherGameObject = otherCollider.attachedRigidbody == null ? otherCollider.gameObject : otherCollider.attachedRigidbody.gameObject;

        IWaterable otherWaterable = 
            this.affectPlayerOnly
                ? otherGameObject.GetComponent<Player>()
                : this.GetWaterable(otherGameObject);
        if (otherWaterable != null)
        {
            ++otherWaterable.Water;

            GameObject.Destroy(this.gameObject);
        }
    }

    private IWaterable GetWaterable(GameObject target)
    {
        MonoBehaviour[] behaviours = target.GetComponents<MonoBehaviour>();
        for (int i = 0; i < behaviours.Length; ++i)
        {
            IWaterable waterable = behaviours[i] as IWaterable;
            if (waterable != null)
            {
                return waterable;
            }
        }

        return null;
    }
}
