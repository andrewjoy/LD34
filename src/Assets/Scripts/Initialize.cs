﻿using UnityEngine;

public sealed class Initialize : MonoBehaviour
{
    [SerializeField]
    private GameObject[] persistentGameObjects = null;

    private void Start()
    {
        foreach (GameObject persistentGameObject in this.persistentGameObjects)
        {
            GameObject.DontDestroyOnLoad(persistentGameObject);
        }
    }
}
