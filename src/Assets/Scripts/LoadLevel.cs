﻿using UnityEngine;

public sealed class LoadLevel : MonoBehaviour
{
    [SerializeField]
    private string levelName = null;

    public void Load()
    {
        Application.LoadLevel(this.levelName);
    }
}
