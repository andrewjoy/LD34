﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Slerpy.Unity3D;

public sealed class FlowerStalk : Stalk
{
    [SerializeField]
    private Effect growEffect = null;

    [SerializeField]
    private float interval = 0.3f;

    [SerializeField]
    private List<StalkWater> targets = null;

    protected override void OnWaterChange(int oldWater, int newWater)
    {
        base.OnWaterChange(oldWater, newWater);

        if (newWater > oldWater)
        {
            this.growEffect.PlayForward();
        }
    }

    protected override void Update()
    {
        base.Update();

        if (this.growEffect.Direction == EffectDirection.Forward && this.targets.Count > 0 && Time.time % this.interval < Time.deltaTime)
        {
            this.targets[0].Target.Water += this.targets[0].Water;

            this.targets.RemoveAt(0);
        }
    }

    [Serializable]
    private sealed class StalkWater
    {
        [SerializeField]
        private Stalk target = null;

        [SerializeField]
        private int water = 1;

        public Stalk Target
        {
            get
            {
                return this.target;
            }
        }

        public int Water
        {
            get
            {
                return this.water;
            }
        }
    }
}
