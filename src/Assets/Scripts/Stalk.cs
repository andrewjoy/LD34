﻿using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

public abstract class Stalk : MonoBehaviour, IWaterable
{
    public enum NeighbourDirection
    {
        North = 0,
        East = 1,
        West = 2,
        South = 3
    }

    [SerializeField]
    private Transform body = null;

    [SerializeField]
    private Transform attachPoint = null;

    [SerializeField]
    private int water = 1;

    [SerializeField]
    private Stalk northNeighbour = null;

    [SerializeField]
    private Stalk eastNeighbour = null;

    [SerializeField]
    private Stalk southNeighbour = null;

    [SerializeField]
    private Stalk westNeighbour = null;

    public Transform Body
    {
        get
        {
            return this.body;
        }
    }

    public Transform AttachPoint
    {
        get
        {
            return this.attachPoint;
        }
    }

    public int Water
    {
        get
        {
            return this.water;
        }

        set
        {
            if (this.water != value)
            {
                if (Application.isPlaying)
                {
                    this.OnWaterChange(this.water, this.water = value);
                }
                else
                {
                    this.water = value;
                }
            }
        }
    }

    public int Height
    {
        get
        {
            return this.water;
        }
    }

    public Stalk NorthNeighbour
    {
        get
        {
            return this.northNeighbour;
        }

        set
        {
            this.northNeighbour = value;
        }
    }

    public Stalk EastNeighbour
    {
        get
        {
            return this.eastNeighbour;
        }

        set
        {
            this.eastNeighbour = value;
        }
    }

    public Stalk SouthNeighbour
    {
        get
        {
            return this.southNeighbour;
        }

        set
        {
            this.southNeighbour = value;
        }
    }

    public Stalk WestNeighbour
    {
        get
        {
            return this.westNeighbour;
        }

        set
        {
            this.westNeighbour = value;
        }
    }

    public Stalk GetNeighbour(NeighbourDirection direction)
    {
        switch (direction)
        {
            case NeighbourDirection.North:
                return this.northNeighbour;
            case NeighbourDirection.East:
                return this.eastNeighbour;
            case NeighbourDirection.West:
                return this.westNeighbour;
            case NeighbourDirection.South:
                return this.southNeighbour;
        }

        return null;
    }

    public void SetNeighbour(NeighbourDirection direction, Stalk newNeighbour)
    {
        if (this != newNeighbour)
        {
            switch (direction)
            {
                case NeighbourDirection.North:
                    this.northNeighbour = newNeighbour;

                    break;
                case NeighbourDirection.East:
                    this.eastNeighbour = newNeighbour;

                    break;
                case NeighbourDirection.West:
                    this.westNeighbour = newNeighbour;

                    break;
                case NeighbourDirection.South:
                    this.southNeighbour = newNeighbour;

                    break;
            }
        }
    }

    public void SetNeighbourMutual(NeighbourDirection direction, Stalk newNeighbour)
    {
        this.SetNeighbour(direction, newNeighbour);

        if (newNeighbour != null)
        {
            newNeighbour.SetNeighbour(direction.Flip(), this);
        }
    }

    protected virtual void OnWaterChange(int oldWater, int newWater)
    {
    }

    protected virtual void Start()
    {
        TransformEffect xEffect = this.body.gameObject.AddComponent<TransformEffect>();

        xEffect.ScaleExtent = new Vector3(0.05f, 0.0f, 0.0f);
        xEffect.SimulatedTime = Random.Range(-xEffect.Duration, xEffect.Duration) * 2.0f;

        TransformEffect yEffect = this.body.gameObject.AddComponent<TransformEffect>();

        yEffect.ScaleExtent = new Vector3(0.0f, 0.0f, 0.05f);
        yEffect.SimulatedTime = Random.Range(-xEffect.Duration, xEffect.Duration) * 2.0f;
    }

    protected virtual void Update()
    {
        this.body.localScale = Vector3.Lerp(
            this.body.localScale, 
            new Vector3(0.8f, this.Height, 0.8f),
            Time.deltaTime);
    }
}

public static class StalkExtensions
{
    public static Stalk.NeighbourDirection Flip(this Stalk.NeighbourDirection direction)
    {
        return (Stalk.NeighbourDirection)(3 - (int)direction);
    }
}
