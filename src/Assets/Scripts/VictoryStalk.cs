﻿using UnityEngine;

public sealed class VictoryStalk : Stalk
{
    [SerializeField]
    private AudioSource victorySoundEffect = null;

    [SerializeField]
    private string nextLevelName = null;

    private float loadNextLevelTime = 0.0f;

    protected override void OnWaterChange(int oldWater, int newWater)
    {
        base.OnWaterChange(oldWater, newWater);

        if (newWater > oldWater && this.loadNextLevelTime <= 0.0f)
        {
            this.loadNextLevelTime = 3.0f;

            foreach (Stalk stalk in Component.FindObjectsOfType<Stalk>())
            {
                stalk.gameObject.AddComponent<RaiseStalk>();
            }

            this.victorySoundEffect.PlayOneShot(this.victorySoundEffect.clip);
        }
    }

    protected override void Update()
    {
        base.Update();

        if (this.loadNextLevelTime > 0.0f)
        {
            this.loadNextLevelTime -= Time.deltaTime;

            if (this.loadNextLevelTime <= 0.0f)
            {
                Application.LoadLevel(this.nextLevelName);
            }
        }
    }

    [RequireComponent(typeof(Stalk))]
    private sealed class RaiseStalk : MonoBehaviour
    {
        private float timeRemaining = 0.0f;

        public Stalk Stalk { get; private set; }

        private void Start()
        {
            this.Stalk = this.gameObject.GetComponent<Stalk>();

            this.AddTime();
        }

        private void Update()
        {
            this.timeRemaining -= Time.deltaTime;

            if (this.timeRemaining <= 0.0f)
            {
                ++this.Stalk.Water;

                this.AddTime();
            }
        }

        private void AddTime()
        {
            this.timeRemaining += Random.Range(0.1f, 0.2f);
        }
    }
}
