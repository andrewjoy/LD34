﻿using UnityEngine;

public sealed class AttachTransform : MonoBehaviour
{
    [SerializeField]
    private Transform target = null;

    [SerializeField]
    private Vector3 offset = Vector3.zero;

    public Transform Target
    {
        get
        {
            return this.target;
        }

        set
        {
            this.target = value;
        }
    }

    private void LateUpdate()
    {
        this.transform.position = this.target.position + new Vector3(this.offset.x * this.target.localScale.x, this.offset.y * this.target.localScale.y, this.offset.z * this.target.localScale.z);
        this.transform.rotation = this.target.rotation;
    }
}
