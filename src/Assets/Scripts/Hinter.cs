﻿using System.Collections.Generic;

using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

public sealed class Hinter : MonoBehaviour
{
    [SerializeField]
    private List<Stalk> stalks = null;

    [SerializeField]
    private GameObject hintPrefab = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H) && this.stalks.Count > 0)
        {
            GameObject hint = (GameObject)GameObject.Instantiate(this.hintPrefab, this.stalks[0].AttachPoint.position, Quaternion.identity);

            hint.transform.parent = this.stalks[0].AttachPoint;
            
            hint.transform.position += new Vector3(0.0f, 0.15f, 0.0f);

            TransformEffect spawnEffect = hint.AddComponent<TransformEffect>();

            spawnEffect.SetWeights(WeightType.Eager, WeightType.Eager);
            spawnEffect.TimeWrap = WrapType.Clamp;
            spawnEffect.Duration = 1.0f;
            spawnEffect.PositionExtent = new Vector3(0.0f, -1.0f / hint.transform.lossyScale.y, 0.0f);

            hint.transform.localPosition += -spawnEffect.PositionExtent;

            this.stalks.RemoveAt(0);
        }
    }
}
