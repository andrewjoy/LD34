﻿using System.Collections.Generic;

using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

[RequireComponent(typeof(Actor))]
public sealed class Player : MonoBehaviour, IWaterable
{
    [SerializeField]
    private GameObject movementAllowedPrefab = null;

    [SerializeField]
    private GameObject movementBlockedPrefab = null;

    [SerializeField]
    private Effect waterGlobePrefab = null;

    [SerializeField]
    private Effect waterSplashPrefab = null;

    [SerializeField]
    private AudioSource soundEffectSource = null;

    [SerializeField]
    private AudioClip jumpUpSound = null;

    [SerializeField]
    private AudioClip jumpDownSound = null;

    [SerializeField]
    private AudioClip pickupSound = null;

    [SerializeField]
    private AudioClip growSoundEffect = null;

    [SerializeField]
    private int water = 0;

    private readonly Stack<Effect> waterGlobes = new Stack<Effect>();

    private readonly GameObject[] movementIndicators = new GameObject[4];

    private float reloadLevelTimeRemaining = 0.0f;

    public int Water
    {
        get
        {
            return this.water;
        }

        set
        {
            this.water = value;
        }
    }

    public IEnumerable<Effect> WaterGlobes
    {
        get
        {
            return this.waterGlobes;
        }
    }

    public Actor Actor { get; private set; }

    public void LoseLevel()
    {
        foreach (Stalk stalk in Component.FindObjectsOfType<Stalk>())
        {
            stalk.Water = 0;
        }

        this.ClearMovementIndicators();

        this.reloadLevelTimeRemaining = 1.5f;
    }

    private void Awake()
    {
        this.Actor = this.gameObject.GetComponent<Actor>();
    }

    private void Start()
    {
        this.DrawMovementIndicators();
    }

    private void Update()
    {
        if (this.reloadLevelTimeRemaining > 0.0f)
        {
            this.reloadLevelTimeRemaining -= Time.deltaTime;

            if (this.reloadLevelTimeRemaining <= 0.0f)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            this.Move(Stalk.NeighbourDirection.West);
        }

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            this.Move(Stalk.NeighbourDirection.East);
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            this.Move(Stalk.NeighbourDirection.North);
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            this.Move(Stalk.NeighbourDirection.South);
        }

        if (Input.GetKeyDown(KeyCode.Space) && this.water > 0)
        {
            --this.water;

            this.Actor.AttachedStalk.Water++;

            Effect addEffect = (Effect)Component.Instantiate(this.waterSplashPrefab, this.Actor.AttachedStalk.AttachPoint.position, Quaternion.identity);

            addEffect.transform.parent = this.Actor.AttachedStalk.AttachPoint;

            GameObject.Destroy(addEffect.gameObject, Mathf.Abs(addEffect.Duration * addEffect.Speed));

            this.soundEffectSource.PlayOneShot(this.growSoundEffect, 1.0f);

            this.DrawMovementIndicators();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        if (this.water > this.waterGlobes.Count)
        {
            Effect newWaterGlobe = (Effect)Component.Instantiate(this.waterGlobePrefab, this.transform.position, this.transform.rotation);

            newWaterGlobe.PlayForward();

            newWaterGlobe.transform.parent = this.transform;

            this.waterGlobes.Push(newWaterGlobe);

            this.soundEffectSource.PlayOneShot(this.pickupSound, 1.0f);
        }

        if (this.water < this.waterGlobes.Count)
        {
            Effect oldWaterGlobe = this.waterGlobes.Pop();

            oldWaterGlobe.PlayBackward();

            GameObject.Destroy(oldWaterGlobe.gameObject, Mathf.Abs(oldWaterGlobe.SimulatedTime * oldWaterGlobe.Speed));
        }
    }

    private void LateUpdate()
    {
        this.DrawMovementIndicators();
    }

    private void Move(Stalk.NeighbourDirection direction)
    {
        int previousHeight = this.Actor.AttachedStalk.Height;

        if (this.Actor.Move(direction))
        {
            this.soundEffectSource.PlayOneShot(previousHeight < this.Actor.AttachedStalk.Height ? this.jumpUpSound : this.jumpDownSound, Random.Range(0.6f, 1.0f));
        }
    }

    private void ClearMovementIndicators()
    {
        for (int i = 0; i < this.movementIndicators.Length; ++i)
        {
            GameObject.Destroy(this.movementIndicators[i]);
        }
    }

    private void DrawMovementIndicators()
    {
        this.ClearMovementIndicators();

        for (int i = 0; i < this.movementIndicators.Length; ++i)
        {
            Stalk.NeighbourDirection direction = (Stalk.NeighbourDirection)i;

            Quaternion targetRotation = Quaternion.identity;

            switch (direction)
            {
                case Stalk.NeighbourDirection.North:
                    targetRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);

                    break;
                case Stalk.NeighbourDirection.East:
                    targetRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);

                    break;
                case Stalk.NeighbourDirection.South:
                    targetRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

                    break;
                case Stalk.NeighbourDirection.West:
                    targetRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);

                    break;
            }

            Stalk movementStalk = this.Actor.CalculateMovementTarget(direction);
            if (movementStalk != null && movementStalk != this.Actor.AttachedStalk)
            {
                this.movementIndicators[i] = (GameObject)GameObject.Instantiate(
                    this.movementAllowedPrefab, 
                    movementStalk.AttachPoint.position + new Vector3(0.0f, 0.2f, 0.0f), targetRotation);
            }
            else if (this.Actor.AttachedStalk != null && (movementStalk = this.Actor.AttachedStalk.GetNeighbour(direction)) != null)
            {
                Vector3 targetPosition = movementStalk.AttachPoint.position;

                targetPosition.y = Mathf.Max(this.Actor.AttachedStalk.AttachPoint.position.y, targetPosition.y) + 0.2f;

                this.movementIndicators[i] = (GameObject)GameObject.Instantiate(this.movementBlockedPrefab, targetPosition, targetRotation);
            }
        }
    }
}
